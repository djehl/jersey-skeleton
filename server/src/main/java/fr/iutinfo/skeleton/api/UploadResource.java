package fr.iutinfo.skeleton.api;

import com.google.common.io.Files;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Path("upload")
public class UploadResource {
    private final static Logger logger = LoggerFactory.getLogger(UploadResource.class);

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public String postForm(@FormDataParam("file") InputStream fileInputStream, @FormDataParam("file") FormDataContentDisposition fileDisposition) throws IOException {
        String fileName = UUID.randomUUID().toString() + "-" + fileDisposition.getFileName();
        String path = System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + fileName;
        logger.debug("Dest file : " + path);
        int fileSize = Math.toIntExact(fileDisposition.getSize());
        writeFile(fileInputStream, path, fileSize);
        return fileName;
    }

    private void writeFile(InputStream inputStream, String fileName, int size) throws IOException {
        byte[] buffer = new byte[size];
        inputStream.read(buffer);
        File targetFile = new File(fileName);
        Files.write(buffer, targetFile);
    }
}
