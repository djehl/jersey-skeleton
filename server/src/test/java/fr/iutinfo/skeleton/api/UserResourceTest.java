package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.UserDto;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.buildDao;
import static fr.iutinfo.skeleton.api.Helper.*;
import static org.junit.Assert.assertEquals;

public class UserResourceTest extends JerseyTest {
    private static final String PATH = "/user";
    private UserDao dao = buildDao(UserDao.class);

    @Override
    protected Application configure() {
        return new Api();
    }

    @Before
    public void init() {
        Helper.initDb();
    }

    @Test
    public void read_should_return_a_user_as_object() {
        User user = createUserWithName("foo");
        UserDto utilisateur = target(PATH + "/" + user.getId()).request().get(UserDto.class);
        assertEquals("foo", utilisateur.getName());
    }

    @Test
    public void read_user_should_return_good_alias() {
        int rmsId = rms().getId();
        UserDto user = target(PATH + "/" + rmsId).request().get(UserDto.class);
        assertEquals("RMS", user.getAlias());
    }

    @Test
    public void read_user_should_return_good_email() {
        int ianId = ian().getId();
        UserDto user = target(PATH + "/" + ianId).request().get(UserDto.class);
        assertEquals("ian@debian.org", user.getEmail());
    }

    @Test
    public void create_should_return_the_user_with_valid_id() {
        UserDto user = new UserDto();
        user.setEmail("thomas");
        Entity<UserDto> userEntity = Entity.entity(user, MediaType.APPLICATION_JSON);
        UserDto savedUser = target(PATH).request().post(userEntity).readEntity(UserDto.class);
        assertEquals(1, savedUser.getId());
    }

    @Test
    public void list_should_return_all_users() {
        createUserWithName("foo");
        createUserWithName("bar");
        List<UserDto> users = target(PATH + "/").request().get(listUserResponseType);
        assertEquals(2, users.size());
    }

    @Test
    public void list_all_must_be_ordered() {
        createUserWithName("foo");
        createUserWithName("bar");
        List<UserDto> users = target(PATH + "/").request().get(listUserResponseType);
        assertEquals("foo", users.get(0).getName());
    }

    @Test
    public void after_delete_read_user_sould_return_204() {
        User u = createUserWithName("toto");
        int status = target(PATH + "/" + u.getId()).request().delete().getStatus();
        assertEquals(204, status);
    }

    @Test
    public void should_delete_user() {
        User u = createUserWithName("toto");
        target(PATH + "/" + u.getId()).request().delete();
        User user = dao.findById(u.getId());
        Assert.assertEquals(null, user);
    }

    @Test
    public void delete_unexisting_should_return_204() {
        int unknowId = 12345;
        int status = target(PATH + "/" + unknowId).request().delete().getStatus();
        assertEquals(204, status);
    }

    @Test
    public void list_should_search_in_name_field() {
        createUserWithName("foo");
        createUserWithName("bar");

        List<UserDto> users = target(PATH + "/").queryParam("q", "ba").request().get(listUserResponseType);
        assertEquals("bar", users.get(0).getName());
    }

    @Test
    public void list_should_search_in_alias_field() {
        rms();
        linus();
        rob();

        List<UserDto> users = target(PATH + "/").queryParam("q", "RMS").request().get(listUserResponseType);
        assertEquals("Richard Stallman", users.get(0).getName());
    }

    @Test
    public void list_should_search_in_email_field() {
        rms();
        linus();
        rob();

        List<UserDto> users = target(PATH + "/").queryParam("q", "fsf").request().get(listUserResponseType);
        assertEquals(2, users.size());
    }
}
