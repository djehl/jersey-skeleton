package fr.iutinfo.skeleton.api;

import org.junit.Test;

import java.security.NoSuchAlgorithmException;

import static fr.iutinfo.skeleton.api.BDDFactory.buildDao;
import static fr.iutinfo.skeleton.api.Helper.createUserWithPassword;
import static org.junit.Assert.*;

public class UserTest {
    private UserDao dao = buildDao(UserDao.class);

    @Test
    public void should_set_salt_at_build() {
        User user = new User();
        assertNotNull(user.getSalt());
        assertFalse(user.getSalt().isEmpty());
    }

    @Test
    public void read_user_should_return_hashed_password() throws NoSuchAlgorithmException {
        createUserWithPassword("Loïc Dachary", "motdepasse", "grain de sable");
        User user = dao.findByName("Loïc Dachary");
        assertEquals("dfeb21109fe5eab1b1db7369844921c44b87b44669b0742f3f73bd166b474779", user.getPasswdHash());
    }

    @Test
    public void read_user_should_read_user_with_same_salt() {
        String expectedSalt = "graindesel";
        createUserWithPassword("Mark Shuttleworth", "motdepasse", expectedSalt);
        User user = dao.findByName("Mark Shuttleworth");
        assertEquals(expectedSalt, user.getSalt());
    }
}
